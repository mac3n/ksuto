#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

/* cut at tabs into a list of parts
 * return the count
 * grow the cut list as necessary
 */
static int cutc = 0;
static char **cutv = NULL;

static int
cuts(char *line) {
	char *s = line;
	int n = 0;
	/* need at least one */
	if (!cutc) {
		cutv = realloc(cutv, ++cutc * (sizeof cutv));
	}
	cutv[n++] = s;
	/* cut at tabs */
	while (*s) {
		switch (*s) {
		case '\n':
			/* strip EOL */
			*s = 0;
			break;
		case '\t':
			/* cut point, grow cutqv as needed */
			if (n == cutc) {
				cutv = realloc(cutv, ++cutc * (sizeof *cutv));
			}
			/* add a cut */
			*s++ = 0;
			cutv[n++] = s;
			break;
		default:
			s++;
		}
	}
	return n;
}

#ifdef TESTS
static void
testcuts(void) {
	int n;
	fprintf(stderr, "test cuts\n");
	n = cuts(strdup("0\t1\t\t33\t4\n"));
	assert(n == 5);
	assert(cutc == 5);
	assert(!strcmp(cutv[0], "0"));
	assert(!strcmp(cutv[1], "1"));
	assert(!strcmp(cutv[2], ""));
	assert(!strcmp(cutv[3], "33"));
	assert(!strcmp(cutv[4], "4"));
}
#endif

/* index range lo <= index < hi
 * negative counts back from end
 * unbound refers to start/end
 */
struct range {
	/* index range */
	int index[2];
	/* index present */
	unsigned char bound[2];
	unsigned char colon;
};

/* bind range to cut count */
static int
rangestart(struct range *r, int n) {
	int index = r->index[0];
	return !r->bound[0] ? 0 : index < 0 ? index + n : index;
}
static int
rangestop(struct range *r, int n) {
	int index = r->index[1];
	return !r->bound[1] ? n : index < 0 ? index + n : index;
}

/* parse a signed index or leave unbound */
static char
*range1(struct range *r, int i, char *s) {
	char sign = 0;
	/* numeric */
	int n = 0;
	r->index[i] = r->bound[i] = 0;
	sign = n = 0;
	for (;;) {
		switch (*s) {
		case '+': case '-':
			if (sign) {
				return NULL;
			}
			sign = *s++;
			continue;
		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9':
			if (!sign) {
				sign = '+';
			}
			n = n * 10 + *s++ - '0';
			r->bound[i] = 1;
			continue;
		}
		break;
	}
	/* finished index */
	r->index[i] = (sign == '-') ? -n : n;
	return s;
}

/* parse an index or range */
static void
range(struct range *r, char *arg) {
	char *s = range1(r, 0, arg);
	r->colon = 0;
	if(s && *s == ':') {
		r->colon = 1;
		s = range1(r, 1, ++s);
	}
	if (!s || *s) {
		fprintf(stderr, "range invalid: %s\n", arg);
		exit(-1);
	}
}

#ifdef TESTS
static void
testrange(void) {
	struct range r;
	fprintf(stderr, "test ranges\n");
	/* index parse */
	range1(&r, 0, "");
	assert(!r.bound[0]);
	range1(&r, 0, "19");
	assert(r.bound[0] && r.index[0] == 19);
	range1(&r, 0, "-11");
	assert(r.bound[0] && r.index[0] == -11);
	/* range parse */
	range(&r, ":");
	assert(!r.bound[0] && r.colon && !r.bound[1]);
	assert(rangestart(&r, 20) == 0 && rangestop(&r, 20) == 20);
	range(&r, "7");
	assert(r.bound[0] && r.index[0] == 7 && !r.colon);
	assert(rangestart(&r, 20) == 7);
	range(&r, "8:");
	assert(r.bound[0] && r.index[0] == 8 && r.colon && !r.bound[1]);
	assert(rangestart(&r, 20) == 8 && rangestop(&r, 20) == 20);
	range(&r, ":9");
	assert(!r.bound[0] && r.colon && r.bound[1] && r.index[1] == 9);
	assert(rangestart(&r, 20) == 0 && rangestop(&r, 20) == 9);
}
#endif

#ifdef TESTS
int main(int argc, char *argv[]) {
	testcuts();
	testrange();
}
#else

int main(int argc, char *argv[]) {
	char *line = NULL;
	size_t linelen = 0;
	/* parse range args */
	int rangec = 0;
	struct range *rangev = NULL;
	rangev = realloc(rangev, (rangec = argc - 1) * sizeof *rangev);
	for (int i = 0; i < argc - 1; i++) {
		range(&rangev[i], argv[i + 1]);
	}
	if (!rangec) {
		fprintf(stderr, "usage:\t%s (index | [index]:[index])...\n", argv[0]);
		return -1;
	}
	/* cut lines and emit ranges */
	for (;;) {
		char sep = 0;
		int n;
		int size = getline(&line, &linelen, stdin);
		if (size < 0) {
			break;
		}
		n = cuts(line);
		for (struct range *r = &rangev[0]; r < &rangev[rangec]; r++) {
			int start = rangestart(r, n);
			int stop = r->colon ? rangestop(r, n) : start + 1;
			for (int i = start; i < stop; ++i) {
				if (0 <= i && i < n) {
					if (sep) {
						fputc(sep, stdout);
					}
					sep = '\t';
					fputs(cutv[i], stdout);
				}
			}
		}
		printf("\n");
	}
	return errno ? -1 : 0;
}
#endif
