`ksuto` is a simple variation on `cut(1)`,
which extracts columns from tab-separated value fields in lines on stdin.
It is most useful extracting columns from large database dumps.

The 
chief differences from `cut` are
*   `ksuto` numbers fields starting at 0, where `cut` starts at 1.
*   `ksuto` uses `:` to indicate a range, where `cut` uses `-`.
`ksuto` ranges include the lower bound but not the upper bound,
where `cut` includes both.
*   `ksuto` can use negative field numbers offset from the end of line,
with `-1` the last field.
*   `ksuto` outputs fields in the order of its arguments (including duplicates),
where `cut` outputs fields in order of appearance.
*   `ksuto` only takes field ranges as arguments, and only processes stdin.

In brief, `ksuto` uses `python` array slice notation.
This includes `:` to select all fields.


Fields not included in a line are omitted.

## USAGE

    usage:	./ksuto (index | [index]:[index])...

## EXAMPLES

Prefix lines by their 4th field, sort lines, and strip the prefix.

    ksuto 3 : | sort | ksuto 1:

Extract the first and last two fields

    ksuto 0 -2:

Extract all but the first and last two fields

    ksuto 1:-2

## BUILD

Use your favorite C compiler.

    cc -Wall -g -O -o ksuto ksuto.c

Build with` -DTESTS` for self-tests.

    cc -Wall -g -DTESTS -o testksuto ksuto.c && ./testksuto

## NOTES

Unlike `cut`, `ksuto` needs to buffer a full line.
